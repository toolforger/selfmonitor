package org.toolforger.selfmonitor.sensors.chained;

import org.junit.jupiter.api.Test;
import org.toolforger.selfmonitor.sensors.elementary.MockSensor;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;


class ExponentialSmoothingSensorTest {


    @Test
    public void testInitialValue() {
        // Arrange
        MockSensor baseValue = new MockSensor(0.0, 4.0);
        ExponentialSmoothingSensor acc = new ExponentialSmoothingSensor(baseValue, 0.75);
        // Act/Assert
        assertThat(acc.poll(), is(3.0));
    }

    @Test
    public void testTwoValues() {
        // Arrange
        MockSensor baseValues = new MockSensor(
                1.0, // Initialize, eventual weight: 1 - 3/16 - 3/4 = 1/16 -> 0.0625 * 1 = 0.0625
                2.0, // eventual weight: (3/4)*(1/4) = 3/16 -> 0.1875 * 2 = 0.375
                3.0  // eventual weight: 3/4 -> 0.75 * 3 = 2.25
        );
        ExponentialSmoothingSensor sensor = new ExponentialSmoothingSensor(baseValues, 0.75);
        // Act
        sensor.poll();
        // Assert
        //   0.0625
        // + 0.375
        // + 2.25
        // = 2.6875
        assertThat(sensor.poll(), is(2.6875));
    }

    @Test
    public void WeightedConstructor() {
        // Arrange
        int sampleCount = 100;
        double weight = 0.75;
        MockSensor baseValues = new MockSensor(
                Arrays.asList(1.0), // Initial value
                sampleCount, 3.0 // sampleCount times 3.0
        );
        ExponentialSmoothingSensor sensor = new ExponentialSmoothingSensor(baseValues, sampleCount, weight);
        // Act: poll (sampleCount - 1) times
        for (int i = 1; i < sampleCount; i++) {
            sensor.poll();
        }
        // Assert
        assertThat(sensor.poll(), is(closeTo(1.0 * (1.0 - weight) + 3.0 * weight, 0.00001)));
    }
}
