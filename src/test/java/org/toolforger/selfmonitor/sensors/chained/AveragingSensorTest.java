package org.toolforger.selfmonitor.sensors.chained;

import org.junit.jupiter.api.Test;
import org.toolforger.selfmonitor.sensors.elementary.MockSensor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class AveragingSensorTest {

    @Test
    public void testRampUp() {
        // Arrange
        MockSensor baseValues = new MockSensor(
                0.0, // Initial value
                3.0, 6.0);
        AveragingSensor sensor = new AveragingSensor(baseValues, 3);
        // Act/Assert
        assertThat(sensor.poll(), is(1.0));
        assertThat(sensor.poll(), is(3.0));
    }

    @Test
    public void testExactFillup() {
        // Arrange
        MockSensor baseValues = new MockSensor(
                0.0, // Initial average
                3.0,  // Buffer content: 0.0, 0.0, 3.0
                5.0,  // Buffer content: 0.0, 3.0, 5.0
                7.0); // Buffer content: 3.0, 5.0, 7.0
        AveragingSensor sensor = new AveragingSensor(baseValues, 3);
        // Act
        sensor.poll();
        sensor.poll();
        // Assert
        assertThat(sensor.poll(), is(5.0));
    }

    @Test
    public void testPhaseOutOfOldValues() {
        // Arrange
        MockSensor baseValues = new MockSensor(
                0.0, // Initial average
                3.0,  // Buffer content: 0.0, 0.0, 3.0
                5.0,  // Buffer content: 0.0, 3.0, 5.0
                7.0,  // Buffer content: 3.0, 5.0, 7.0
                9.0); // Buffer content: 5.0, 7.0, 9.0
        AveragingSensor sensor = new AveragingSensor(baseValues, 3);
        sensor.poll();
        sensor.poll();
        sensor.poll();
        // Assert
        assertThat(sensor.poll(), is(7.0));
    }
}
