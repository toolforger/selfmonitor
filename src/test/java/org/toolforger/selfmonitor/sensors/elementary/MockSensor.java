package org.toolforger.selfmonitor.sensors.elementary;

import org.toolforger.selfmonitor.sensors.Sensor;

import java.util.Iterator;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class MockSensor extends Sensor {

    public final Iterator<Double> it;

    /**
     * A mock that will return the given {@code values}, then become inactive.
     */
    public MockSensor(double... values) {
        it = DoubleStream.of(values).iterator();
    }

    /**
     * A mock that will return {@code value}, {@code count} times, then become inactive.
     */
    public MockSensor(int count, double value) {
        it = DoubleStream.generate(() -> value).limit(count).iterator();
    }

    public MockSensor(List<Double> values, int count, double value) {
        it =
            Stream.concat(
                    values.stream(),
                    Stream.generate(() -> value).limit(count)
            ).iterator();
    }

    @Override
    public boolean isActive() {
        return it.hasNext();
    }

    @Override
    public double poll() {
        return it.next();
    }
}
