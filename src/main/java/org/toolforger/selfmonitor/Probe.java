package org.toolforger.selfmonitor;

import org.toolforger.selfmonitor.sensors.Sensor;

import java.util.function.DoubleConsumer;

/**
 * A {@link Probe} makes sure that its {@link Sensor} is polled regularly.
 */
public class Probe extends Thread {

    private final Sensor sensor;
    private final int pollingIntervalMillis;
    private final DoubleConsumer consumer;

    public Probe(Sensor sensor, int pollingIntervalMillis, DoubleConsumer consumer) {
        this.sensor = sensor;
        this.pollingIntervalMillis = pollingIntervalMillis;
        this.consumer = consumer;
        setName(toString());
        setDaemon(true);
        start();
    }

    @Override
    public void run() {
        while (sensor.isActive() && !Thread.currentThread().isInterrupted()) {
            consumer.accept(sensor.poll());
            try {
                //noinspection BusyWait
                sleep(pollingIntervalMillis);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public String toString() {
        return
            "Probe{" +
            "sensor=" + sensor +
            ", pollingIntervalMillis=" + pollingIntervalMillis +
            ", consumer=" + consumer +
            '}';
    }
}
