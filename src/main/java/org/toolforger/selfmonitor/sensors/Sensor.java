package org.toolforger.selfmonitor.sensors;

import org.apache.commons.lang3.builder.ToStringBuilder;

public abstract class Sensor {

    /**
     * @return Can #poll still be usefully called?
     */
    public abstract boolean isActive();

    /**
     * Make a measurement and act on it.
     */
    public abstract double poll();

    @Override
    public String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this);
        appendFields(tsb);
        return tsb.toString();
    }

    public void appendFields(ToStringBuilder tsb) {
        tsb.append("active", isActive());
    }
}
