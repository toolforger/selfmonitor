package org.toolforger.selfmonitor.sensors.elementary;

import com.sun.management.OperatingSystemMXBean;
import org.toolforger.selfmonitor.sensors.Sensor;

import java.lang.management.ManagementFactory;

public class CpuTimeSensor extends Sensor {

    // //// Class initialization

    /**
     * @return CPU time used by the current JVM instance since start, in nanoseconds.<br>
     *         May throw exceptions such as {@link ClassNotFoundException} if used in a JDK that does not have
     *         {@code com.sun.management.OperatingSystemMXBean}, or {@link NoSuchMethodException} and similar if
     *         {@code OperatingSystemMXBean} exists but is incompatible.
     */
    private static long getCpuTimeNanos() {
        OperatingSystemMXBean b = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        return b.getProcessCpuTime();
    }

    /**
     * Throwable that happened during class initializiation, if any.<br>
     * This is made available so that clients can log any problems if they want/need to.
     */
    private static final Throwable loadTimeError;

    /**
     * @return Any {@link Exception} ({@link Throwable}, actually) that happened during class initialization.<br>
     *         The sensor is inactive if this is not {@code null}.
     */
    public static Throwable getLoadTimeError() {
        return loadTimeError;
    }

    static {
        Throwable temp; // Need to assign here because Java disallows assigning to final in both try and catch branch.
        try {
            // Assume the call works if there is no excepton.
            getCpuTimeNanos();
            temp = null;
        } catch (Throwable t) {
            // We expect ClassNotFoundException or NoSuchMethodException here, but it is possible that unknown JDKs
            // throw an unexpected exception, and we don't want the class to go into a "wasn't initialized" state, we
            // want to have it normally loaded and report that it cannot make measurements.
            temp = t;
        }
        loadTimeError = temp;
    }

    @Override
    public boolean isActive() {
        return loadTimeError != null;
    }

    private static final double NANOS_TO_MILLIS = 1_000_000.0;

    public double poll() {
        return getCpuTimeNanos() / NANOS_TO_MILLIS;
    }
}
