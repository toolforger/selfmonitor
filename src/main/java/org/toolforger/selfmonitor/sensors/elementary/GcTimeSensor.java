package org.toolforger.selfmonitor.sensors.elementary;

import org.toolforger.selfmonitor.sensors.Sensor;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;

/**
 * <p>
 *     A {@link Sensor} that reports the percentage of time spent in GC since the JVM started, in milliseconds.
 * </p>
 */
public class GcTimeSensor extends Sensor {

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public double poll() {
        long result = 0;
        for (GarbageCollectorMXBean b: ManagementFactory.getGarbageCollectorMXBeans()) {
            long t = b.getCollectionTime();
            if (t != -1) {
                result += t;
            }
        }
        return result;
    }
}
