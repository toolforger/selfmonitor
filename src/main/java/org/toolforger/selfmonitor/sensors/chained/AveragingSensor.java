package org.toolforger.selfmonitor.sensors.chained;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.toolforger.selfmonitor.sensors.Sensor;

/**
 * <p>
 *     This {@link ChainedSensor} averages the last {@code N} {@link #poll()}ed values.
 * </p>
 */
public class AveragingSensor extends ChainedSensor {

    private final double [] samples;
    private int nextSlot;
    private volatile double value;

    /**
     * Immediately polls {@code baseSensor} and assumes that there were {@code sampleCount} previous measurements with
     * that same value.
     */
    public AveragingSensor(Sensor baseSensor, int sampleCount) {
        super(baseSensor);
        samples = new double[sampleCount];
        value = baseSensor.poll();
    }

    @Override
    public double poll() {
        double newValue = baseSensor.poll();
        value = value - samples[nextSlot] + newValue;
        samples[nextSlot] = value;
        nextSlot = (nextSlot + 1) % samples.length;
        return value / samples.length;
    }

    @Override
    public void appendFields(ToStringBuilder tsb) {
        tsb.append("sampleCount", samples.length);
        super.appendFields(tsb);
    }
}
