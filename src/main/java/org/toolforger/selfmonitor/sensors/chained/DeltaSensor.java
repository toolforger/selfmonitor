package org.toolforger.selfmonitor.sensors.chained;

import org.toolforger.selfmonitor.sensors.Sensor;

/**
 * <p>
 *     Reports the delta between current and previous value of another sensor.
 * </p>
 * <p>
 *     This is useful for transforming resource usage counters in used-per-measurement-interval sensors.
 * </p>
 * <p>
 *     Note that the base {@link Sensor} is polled once in the constructor, because many usage counters start with a
 *     nonzero value and we want the first measurement to cover just the delta since {@link DeltaSensor} construction.
 * </p>
 */
public class DeltaSensor extends ChainedSensor {

    private double previousValue;

    public DeltaSensor(Sensor baseSensor) {
        super(baseSensor);
        poll();
    }

    @Override
    public double poll() {
        double polledValue = baseSensor.poll();
        double result = polledValue - previousValue;
        previousValue = polledValue;
        return result;
    }
}
