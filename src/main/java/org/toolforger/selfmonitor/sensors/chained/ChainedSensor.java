package org.toolforger.selfmonitor.sensors.chained;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.toolforger.selfmonitor.sensors.Sensor;

/**
 * A {@link Sensor} that uses a single other sensor as a base.
 */
public abstract class ChainedSensor extends Sensor {

    protected Sensor baseSensor;

    public ChainedSensor(Sensor baseSensor) {
        this.baseSensor = baseSensor;
    }

    @Override
    public boolean isActive() {
        return baseSensor.isActive();
    }

    /**
     * <p>
     *     Used by {@link Sensor#toString()} to append fields to {@code tsb}.
     * </p>
     * <p>
     *     This function is intended to be called from overrides of {@link Sensor#toString()} implementations, for any
     *     {@link Sensor} target.<br/>
     *     Java has no way to express this restriction ({@code protected} allows only {@code super} calls or if it is in
     *     the same package, and we want allow delegating to {@code Sensor}s from third-party packages).
     * </p>
     */
    public void appendFields(ToStringBuilder tsb) {
        tsb.append("baseSensor", baseSensor);
        super.appendFields(tsb);
    }
}
