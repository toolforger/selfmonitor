package org.toolforger.selfmonitor.sensors.chained;

import org.toolforger.selfmonitor.sensors.Sensor;

/**
 * <p>
 *     Reports a sensor value, divided by the time since last measurement.
 *     <br>
 *     The result can typically be interpreted as consumption per second.
 * </p>
 */
public class TimeIntervalSensor extends ChainedSensor {

    private long previousTimeMillis;

    private double previousValue;

    private static final double MILLISECONDS_PER_SECOND = 1000.0;

    public TimeIntervalSensor(Sensor baseSensor) {
        super(baseSensor);
        previousTimeMillis = System.currentTimeMillis();
        previousValue = baseSensor.poll();
    }

    @Override
    public double poll() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis == previousTimeMillis) {
            // Same timestamp as previous poll, we don't have any new data (and we would divide by zero if we tried).
            // Just return the previous value.
            return previousValue;
        }
        double result = baseSensor.poll() / (currentTimeMillis - previousTimeMillis) * MILLISECONDS_PER_SECOND;
        previousTimeMillis = currentTimeMillis;
        previousValue = result;
        return result;
    }
}
