package org.toolforger.selfmonitor.sensors.chained;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.toolforger.selfmonitor.sensors.Sensor;

/**
 * <p>
 *     A {@link Sensor} that applies a first-order exponential smoothing function to its base {@code Sensor}.
 * </p>
 * <p>
 * Properties:
 * </p>
 * <ul>
 *     <li>
 *         It is a moving average where previous values decrease in weight exponentially instead of being discarded.
 *     </li>
 *     <li>
 *         It lags behind any trend that's in the data. (Keep this in mind when you need to react very quickly, or even
 *         ahead of time.)
 *     </li>
 *     <li>
 *         It is parameterized with the a "smoothing Factor" {@code α} in the {@code 0.0...1.0} range.
 *     </li>
 *     <li>
 *         "Smoothing factor" is a (historic) misnomer, it is actually the weight for the newest value. So:<br/>
 *         A lower smoothing factor means a smoother curve but more lag.<br/>
 *         A higher smoothing factor means a jaggier curve but less lag.<br/>
 *     </li>
 *     <li>
 *         It {@link #poll()}s right at construction time and initializes its current average from there.<br>
 *         Mathematically, this means it assumes an infinity of previous polls that returned that value.
 *     </li>
 * </ul>
 * <p>
 *
 * <p>
 *     <b>Algorithm</b>
 * </p>
 * <p>
 *     With every new input, the value gets updated to {@code newValue = (1-α)*previousValue + α*newInput}.
 * </p>
 * <p>
 *     Implications:
 * </p>
 * <ul>
 *     <li>
 *         The newest {@code N} values have the following individual weights:<br/>
 *         {@code 0}: {@code α}<br/>
 *         {@code 1}: {@code (1 - α)*α}<br/>
 *         {@code 2}: {@code (1 - α)}<sup>{@code 2}</sup>{@code *α}<br/>
 *         ...<br/>
 *         {@code N}: {@code (1 - α)}<sup>{@code N}</sup>{@code *α}<br/>
 *     </li>
 *     <li>
 *         Values {@code -∞}...{@code N} have a collective weight of {@code (1-α)}<sup>{@code N}</sup> (for
 *         {@code N > 0}, not for {@code }N = 0}).<br/>
 *         Values {@code N+1}...{@code 0} conversely have a collective weight of {@code 1 - (1-α)}<sup>{@code N}</sup>
 *     </li>
 * </ul>
 */
public class ExponentialSmoothingSensor extends ChainedSensor {

    private final double alpha;
    private final double antiAlpha;
    private double value;

    public ExponentialSmoothingSensor(Sensor baseSensor, double alpha) {
        super(baseSensor);
        this.alpha = alpha;
        antiAlpha = 1.0 - alpha;
        value = baseSensor.poll();
    }

    /**
     * Create an accumulator where the newest {@code sampleCount} samples together have a weight of {@code weight}.
     */
    public ExponentialSmoothingSensor(Sensor baseSensor, int sampleCount, double weight) {
        // w = 1 - (1 - a)^c
        // (1 - a)^c = 1 - w
        // 1 - a = rootC(1 - w)
        // 1 - rootC(1 - w) = a
        this(baseSensor, 1.0 - Math.pow(1.0 - weight, 1.0 / sampleCount));
    }

    @Override
    public double poll() {
        double baseSensorValue = baseSensor.poll();
        value = antiAlpha * this.value + alpha * baseSensorValue;
        return value;
    }

    @Override
    public void appendFields(ToStringBuilder tsb) {
        tsb.append("alpha", alpha);
        super.appendFields(tsb);
    }
}
