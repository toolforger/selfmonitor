package org.toolforger.selfmonitor.sensors.composite;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.toolforger.selfmonitor.sensors.Sensor;

/**
 * A {@link Sensor} that reports the ratio of two other {@code Sensor}s.
 */
public class RatioSensor extends Sensor {

    private final Sensor numerator;
    private final Sensor denominator;

    public RatioSensor(Sensor numerator, Sensor denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @Override
    public boolean isActive() {
        return numerator.isActive() && denominator.isActive();
    }

    @Override
    public double poll() {
        return numerator.poll() / denominator.poll();
    }

    @Override
    public void appendFields(ToStringBuilder tsb) {
        tsb.append("numerator", numerator);
        tsb.append("denominator", denominator);
    }
}
