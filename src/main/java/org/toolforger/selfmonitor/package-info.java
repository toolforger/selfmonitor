/**
 * <p>
 *     Application self-monitoring, using {@link org.toolforger.selfmonitor.sensors.Sensor Sensor}s, which fall into the
 *     following categories:
 * </p>
 * <uL>
 *     <li>
 *         {@link org.toolforger.selfmonitor.sensors.elementary elementary}, which directly measure some {@code double}
 *         value.
 *     </li>
 *     <li>
 *         {@link org.toolforger.selfmonitor.sensors.chained chained}, which wrap some other
 *         {@link org.toolforger.selfmonitor.sensors.Sensor Sensor} but postprocess the value.
 *     </li>
 *     <li>
 *         {@link org.toolforger.selfmonitor.sensors.composite composite}, which combine two or more
 *         {@link org.toolforger.selfmonitor.sensors.Sensor Sensor} values into a single value.
 *     </li>
 * </uL>
 * <p>
 *     The other component is the {@code Probe}s, which is a {@link java.lang.Thread Thread} that regularly polls a
 *     {@link org.toolforger.selfmonitor.sensors.Sensor Sensor}<br/>
 *     This allows taking measurements without littering the application code with
 *     {@link org.toolforger.selfmonitor.sensors.Sensor#poll() Sensor#poll()} calls.<br/>
 *     The {@code Thread} is in daemon mode so it does not prevent application exit.<br/>
 *     It is recommended to run it at a higher priority than the main code, do avoid starving the measurements when the
 *     main application starts hogging resources.
 * </p>
 * <p>
 *     <b>Note:</b> {@link org.toolforger.selfmonitor.sensors.Sensor Sensor}s are built to deal just with
 *     {@code double}-values measurements.<br/>
 *     This is a by-product of no use case for non-{@code double} measurements having presented itself yet, not a
 *     reasoned design decision.
 * </p>
 * <p>
 *     Feel free to create your own {@link org.toolforger.selfmonitor.sensors.Sensor}s.<br/>
 *     Any submissions will be considered for inclusion!
 * </p>
 */
package org.toolforger.selfmonitor;
