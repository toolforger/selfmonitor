package org.toolforger.selfmonitor.sensors.elementary;

import org.toolforger.selfmonitor.sensors.Sensor;

import java.util.ArrayList;
import java.util.Random;

/**
 * <p>
 *     This program keeps allocating and forgetting data chunks.<br/>
 *     It allocates 1MB-sized chunks of memory until it hits an out-of-memory condition, then alternates between
 *     allocating and deallocating.
 * </p>
 * <p>
 *     To use it as a test, check that the output roughly matches what some other Java monitoring tool reports (e.g.
 *     VisualVM).<br/>
 *     Note that there may be variations because measurement interval and sampling times may differ; it's probably best
 *     to average over the last few cycles.
 * </p>
 * <p>
 *     Observations:
 * </p>
 * <ul>
 *     <li>
 *         Sometimes it will crash with an {@link OutOfMemoryError}.<br/>
 *         Might be some background process allocating memory immediately before the memory eater deallocates, but your
 *         speculation is likely as good as mine. Since we're trying to observe the GC time sensor and whether its
 *         output roughly matches what any other Java tool reports (VisualVM or whatever else one can use).
 *     </li>
 * </ul>
 */
class TestGCTimeSensor {

    /**
     * Approximate chunk size to allocate.
     */
    private static final int CHUNK_SIZE = 1024 * 1024;

    /**
     * Number of chunks to release after hitting the memory limit, so the JVM has enough space to continue running.
     */
    private static final int CHUNKS_TO_RELEASE = 6;

    public static void main(String... args) {
        // Use up memory until we hit an out-of-memory error
        ArrayList<byte[]> memoryEater = new ArrayList<>();
        while (true) {
            try {
                memoryEater.add(new byte[CHUNK_SIZE]);
            } catch (OutOfMemoryError ignored) {
                break;
            }
        }
        System.out.println("Allocated " + memoryEater.size() + " chunks à 1MB.");
        // Deallocate some chunks so that we don't use up all memory.
        for (int i = 0; i < CHUNKS_TO_RELEASE; i++){
            memoryEater.remove(memoryEater.size() - 1);
        }
        // Garbage Collection sensor
        Sensor sensor = new GcTimeSensor();
        // Run it
        Random random = new Random();
        long nextReport = 0;
        //noinspection InfiniteLoopStatement
        while (true) {
            memoryEater.set(random.nextInt(memoryEater.size()), new byte[CHUNK_SIZE]);
            long currentTime = System.currentTimeMillis();
            if (currentTime >= nextReport) {
                System.out.println(sensor.poll());
                nextReport = currentTime + 1000; // Report once per second
            }
        }
    }
}
